from flask import Flask, render_template, url_for

app = Flask(__name__, static_folder='static')

@app.route('/')
def hello_world():
	return render_template ('snake.html')

if __name__== '__main__':
	app.run('0.0.0.0', port="5001")
